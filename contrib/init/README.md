Sample configuration files for:

SystemD: penetrationcoind.service
Upstart: penetrationcoind.conf
OpenRC:  penetrationcoind.openrc
         penetrationcoind.openrcconf
CentOS:  penetrationcoind.init
OS X:    org.penetrationcoin.penetrationcoind.plist

have been made available to assist packagers in creating node packages here.

See doc/init.md for more information.
